def turn_on(switch_num):
    switches[switch_num - 1] = True
    return switches[switch_num - 1]
        
def turn_off(switch_num):
    switches[switch_num - 1] = False
    return switches[switch_num - 1]
    
def auto_turn_on(automatic_on):
    if any(current_time in auto_turn for auto_turn in automatic_on):
        turn_on(auto_turn[0])
        
def auto_turn_off(automatic_on, automatic_off):
    if any(current_time in auto_turn for auto_turn in automatic_off):
        if any(auto_turn in automatic_on for auto_turn in automatic_off):
            if automatic_off.count(auto_turn) >= 2:
                turn_off(auto_turn[0])
            else:
                turn_on(auto_turn[0])
        else:
            turn_off(auto_turn[0])
                
def turn_at_time(command_parts):
    if current_time == time:
        if command_parts[1] == "ON":
            turn_on(switch_num)
        else:
            if not auto_turn_off(automatic_on, automatic_off):
                turn_off(switch_num)
                
switches = [False] * 16
current_time = 0000
automatic_on = []
automatic_off = []


input_list = ['1 ON' , 'TIME 0700' , '? 1' , '2 ON' , 'TIME 1300' , '2 OFF AT 1300' , '? ALL' , 'STOP']

for command in input_list:
    command_parts = command.split()
    
    if command_parts[0] == "TIME":
        current_time = int(command_parts[1])
        auto_turn_on(automatic_on)
        auto_turn_off(automatic_on, automatic_off)

    elif command_parts[0] == "?" and command_parts[1] == "ALL":
        for switch_num in range(len(switches)):
            if switches[switch_num]:
                print("Switch {} : ON".format(switch_num + 1))
            else:
                print("Switch {} : OFF".format(switch_num + 1))
          
        
    elif command_parts[0] == "?" and int(command_parts[1]) in range(16):
        switch_num = int(command_parts[1]) - 1
        if switches[switch_num]:
            print("Switch {} : ON".format(switch_num + 1))
        else:
            print("Switch {} : OFF".format(switch_num + 1))      
   

    elif command_parts[0] == "STOP":
        print("SIMULATION END")
        exit()
    
    elif int(command_parts[0]) in range(16) and len(command_parts) >= 3:
        switch_num = int(command_parts[0])
        time = int(command_parts[3])
        auto_turn = []
        auto_turn.append(switch_num)
        auto_turn.append(time)
        
        turn_at_time(command_parts)
                  
        if command_parts[1] == "ON":
            automatic_on.append(auto_turn)
        else:
            automatic_off.append(auto_turn)
            
    else:
        switch_num = int(command_parts[0])
        input_pair = [int(command_parts[0]), current_time]
        if command_parts[1] == "ON" and input_pair not in automatic_off:
            turn_on(switch_num)
        elif command_parts[1] == "OFF" and input_pair not in automatic_on:
            turn_off(switch_num)